# dockerDemo

dockerDemo for fronted-VueCLI

## 1. start Container
> docker-compose up -d (-d: detach，表示執行完立刻將使用權回到 CLI)
## 2. exec Container
> docker exec -it MyVue bash

## 3. create project
> cd vue

> vue create projectName

## 4. start Vue
> cd projectName

> npm run serve

## 5. open Web
> url => http://localhost:9090/